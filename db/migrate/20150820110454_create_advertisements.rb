class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.references :user, index: true
      t.references :city, index: true
      t.references :district, index: true

      t.timestamps null: false
    end
    add_foreign_key :advertisements, :users
    add_foreign_key :advertisements, :cities
    add_foreign_key :advertisements, :districts
  end
end
