# == Schema Information
#
# Table name: advertisements
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  city_id     :integer
#  district_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Advertisement < ActiveRecord::Base
  belongs_to :user
  belongs_to :city
  belongs_to :district
end
