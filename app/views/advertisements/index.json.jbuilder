json.array!(@advertisements) do |advertisement|
  json.extract! advertisement, :id, :user_id, :city_id, :district_id
  json.url advertisement_url(advertisement, format: :json)
end
